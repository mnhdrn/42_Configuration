# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    INSTALL.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/07 22:00:52 by clrichar          #+#    #+#              #
#    Updated: 2019/03/13 15:49:22 by clrichar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#=======================Re-install Brew
curl -fsSL https://rawgit.com/kube/42homebrew/master/install.sh | zsh

#=======================Tool
brew install bash neovim ncdu
pip3 install neovim
# :UpdateRemotePlugins

#=======================Tmux
brew install tmux reattach-to-user-namespace

#=======================Valgrind
brew install valgrind

#=======================Vim
cp ./0-vim/vimrc $HOME/.vimrc
cp -r ./0-vim/vim $HOME/.vim

#=======================Tmux
cp ./1-tmux/tmux.conf $HOME/.tmux.conf
cp -r ./1-tmux/tmux $HOME/.tmux

#=======================Zsh theme
cp ./2-zsh/zshrc $HOME/.zshrc
cp -r ./2-zsh/ocha-zsh $HOME/.ocha-zsh

#======================DECOMP
cd $HOME/.vim/bundle/
tar xzvf $HOME/.vim/bundle/syntastic.tar.z
tar xzvf $HOME/.vim/bundle/high.tar.z
tar xzvf $HOME/.vim/bundle/emmet-vim.tar.z
tar xvf $HOME/.vim/bundle/phpindent.tar.z

cd $HOME/.tmux/plugins/
tar xvf $HOME/.tmux/plugins/tmux-fingers.tar
tar xvf $HOME/.tmux/plugins/tmux-yank.tar
