"____________________________________"
:set number relativenumber
:set cc=80
:set clipboard=unnamedplus
:set noswapfile
:set ignorecase
:set smartcase

"____________________________________"
let g:impact_transbg=1

"___________________________________"
:syntax on
:colorscheme codedark

"___________________________________"
:set list
:set showbreak=↪
:set listchars=tab:→\ ,trail:·,eol:¬,extends:…,precedes:…
hi NonText ctermfg=8
hi SpecialKey ctermfg=8

"___________________________________"
:set tabstop=4
:set softtabstop=4
:set shiftwidth=4
:set smartindent
:set autoindent
:set scrolloff=10
"____________________________________"
:set wildmenu
:set incsearch
:set hlsearch

"____________________________________"
:set cursorline
":set mouse=a
:set tabpagemax=100

"____________________________________"
:syntax sync minlines=300
:set synmaxcol=100
:set regexpengine=1

"____________________________________"
"filetype plugin indent on
call plug#begin()
	Plug 'neomake/neomake'
	Plug 'mattn/emmet-vim'
	Plug '2072/PHP-Indenting-for-VIm'
	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
	Plug 'junegunn/goyo.vim'
	Plug 'OCamlPro/ocp-indent'
call plug#end()

let g:deoplete#enable_at_startup = 1
let g:deoplete#complete_method = "complete"
let g:deoplete#ignore_sources = {}
let g:deoplete#ignore_sources.ocaml = ['buffer', 'around', 'member', 'tag']
let g:deoplete#auto_complete_delay = 0
autocmd CompleteDone * silent! pclose!

"____________________________________"
autocmd BufWinEnter,BufWritePost *.c :Neomake gcc

call neomake#configure#automake('w')
let g:neomake_open_list=0
let g:c_syntax_for_h=1
let g:neomake_c_enabled_makers=['gcc']
let g:neomake_gcc_args=[
		\ '-fsyntax-only',
		\ '-Wall',
		\ '-Werror',
		\ '-Wextra',
		\ '-Wconversion',
		\ '-Wunreachable-code',
		\ '-Winit-self',
		\ '-I../includes',
		\ '-I../include',
		\ '-I.',
		\ ]
"	\ '-Wfloat-equal',
"	\ '-Wshadow',
"	\ '-Wpointer-arith',
"	\ '-Wcast-align',
"	\ '-Wstrict-prototypes',
"	\ '-Wwrite-strings',
"	\ '-Waggregate-return',

"____________________________________"
"Goyo Conf
let g:goyo_width = 120
let g:goyo_height = 90
let g:goyo_linenr = 1

function! s:goyo_enter()
	:set nolist
endfunction

function! s:goyo_leave()
	:set list
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

"____________________________________"
:if (&ft=='python')
:ts=4
:syn match pythonBoolean "\(\W\|^\)\@<=self\(\.\)\@="
:let python_highlight_all = 1
:endif


:set rtp+=/Users/clrichar/.opam/default/share/merlin/vim

:if (&ft=='ocaml')
:ts=2
let g:neomake_c_enabled_makers=['merlin']
:autocmd BufWritePost *.ml execute MerlinErrorCheck | redraw!
:endif

"____________________________________"
"Only apply to .txt files...
augroup HelpInTabs
	autocmd!
	autocmd BufEnter	*.txt	call HelpInNewTab()
augroup END

"Only apply to help files...
function! HelpInNewTab ()
	if &buftype == 'help'
		"Convert the help window to a tab...
		execute "normal \<C-W>T"
	endif
endfunction
"____________________________________"
autocmd FileType ruby,erb,inky,inky-erb   setlocal tabstop=8 shiftwidth=2 expandtab
"____________________________________"
autocmd FileType html,css,php EmmetInstall
let g:user_emmet_install_global = 0
"____________________________________"
function! CommentToggle()
	execute ':silent! s/\([^ ]\)/\/\/ \1/'
	execute ':silent! s/^\( *\)\/\/ \/\/ /\1/'
endfunction


"smart indent when entering insert mode with i on empty lines"
function! IndentWithI()
	if len(getline('.')) == 0
		return "\"_cc"
	else
		return "i"
	endif
endfunction

function ToggleTheGoyo()
	if exists('#Goyo')
		set list
	else
		set nolist
	endif

endfunction

"____________________________________"
let g:BASH_Ctrl_j = 'off'
let g:BASH_Ctrl_s = 'off'

:map <C-n> :Texplore<CR>
":map f <Plug>Sneak_f
":map F <Plug>Sneak_F
":map t <Plug>Sneak_t
":map T <Plug>Sneak_T
:nnoremap  M :set list!<bar>:Goyo<CR>
:nnoremap <space> :noh<CR>
:nnoremap <S-j> :tabn<CR>
:nnoremap <S-k> :tabp<CR>
:nnoremap <F3> :Stdheader<CR>
:nnoremap <expr> i IndentWithI()
:nnoremap <C-k> :m.+1<CR>==
:nnoremap <C-j> :m.-2<CR>==
:vnoremap <C-k> :m'>+1<CR>gv=gv
:vnoremap <C-j> :m'<-2<CR>gv=gv
:cmap W MerlinErrorCheck

"____________________________________"
"
" Use I to enter in insert at the begining of the line
" Use A to enter in insert at the ending of the line
"
" Go to the top of the file gg
" Go to the end of the file G
"
" Use :terminal to open a terminal in vim
"
" A to enter in insert mode at the begining of the line
" I to enter in insert mode at the end of the line
"
" Center screen "normal zz
" Go to top screan H
" Go to bottom of screen L
" Go to center of screen M
" Make down screen ctrl + e
" Make up screen ctrl + y
"
" Go to the begining of line 0
" Go to the end of line $
" Go to first char of line ^
" Go to the last char of line g_
"
" Go to the next choosen character f(x)
" Go to the previous choosen character F(x)
" Repeat previous f/F in the same way ;
" Repeat previous f/F in reversed way ,
"
" Set lowercase gu
" Set uppercase gU
"
" Use * to seek the word where you are
" w to go to the next word, e to go to the end of the next word
" b to go to the previous word
"
" Use :set filetype=php || html for indentation in php
